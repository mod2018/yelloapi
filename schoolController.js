// schoolController.js

// Import schoolRoute model
SchoolRoute = require('./schoolModel');

// Handle index actions
exports.index = function (req, res) {
    SchoolRoute.get(function (err, schoolRoute) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Schools retrieved successfully",
            data: schoolRoute
        });
    });
};

// Handle create schoolRoute actions
exports.new = function (req, res) {
    var schoolRoute = new SchoolRoute();
    schoolRoute.name = req.body.name;
    schoolRoute.code = req.body.code;

// save the schoolRoute and check for errors
    schoolRoute.save(function (err) {
        // if (err)
        //     res.json(err);

res.json({
            message: 'New School created!',
            data: schoolRoute
        });
    });
};

// Handle view schoolRoute info
exports.view = function (req, res) {
    SchoolRoute.findOne({code: req.params.schoolRoute_code}, function (err, schoolRoute) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'School details loading..',
            data: schoolRoute
        });
    });
};

// Handle update schoolRoute info
exports.update = function (req, res) {

SchoolRoute.findOne({code: req.params.schoolRoute_code}, function (err, schoolRoute) {
        if (err)
        res.send(err);
        schoolRoute.name = req.body.name ? req.body.name : schoolRoute.name;
        schoolRoute.code = req.body.code ? req.body.code : schoolRoute.code;



// save the schoolRoute and check for errors
        schoolRoute.save(function (err) {
            if (err)
            res.status(500).send(err);
            res.status(200).json({
                message: 'School Info updated',
                data: schoolRoute
            });
        });
    });
};

// Handle delete schoolRoute
exports.delete = function (req, res) {
    SchoolRoute.remove({
        _id: req.params.schoolRoute_id
    }, function (err, schoolRoute) {
        if (err)
            res.end(err);

res.json({
            status: "success",
            message: 'School deleted'
        });
    });
};
