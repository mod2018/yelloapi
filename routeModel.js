// routeModel.js
var mongoose = require('mongoose');

// Setup schema
var routeSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    school: {
        type: String,
        required: true
    },
    code: {
        type: String,
        required: true
    },
    active: {
        type: Boolean,
        required: true,
        default: false
    },
    latitude: {
        type: String
    },
    longitude: {
        type: String
    },
    create_date: {
        type: Date,
        default: Date.now
    }
});

// Export String model
var BusRoute = module.exports = mongoose.model('busRoute', routeSchema);

module.exports.get = function (callback, limit) {
    BusRoute.find(callback).limit(limit);
}
