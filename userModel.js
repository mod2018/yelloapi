// userModel.js

var mongoose = require('mongoose');

// Setup schema
var userSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    authCode: {
        type: String,
        required: false
    },
    authKey: {
      type: String,
      required: false
    },
    authorizedSchools:{
      type: String,
      required: false,
      default: null
    },
    subscription: {
      type: String,
      required: false
    },
    create_date: {
        type: Date,
        default: Date.now
    }
});

// Export String model
var UserRoute = module.exports = mongoose.model('userRoute', userSchema);

module.exports.get = function (callback, limit) {
    UserRoute.find(callback).limit(limit);
}
