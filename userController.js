// userController.js

// Import userRoute model
UserRoute = require('./userModel');

// Handle index actions
exports.index = function (req, res) {
    UserRoute.get(function (err, userRoute) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Users retrieved successfully",
            data: userRoute
        });
    });
};

// Handle create userRoute actions
exports.new = function (req, res) {
    var userRoute = new UserRoute();
    userRoute.name = req.body.name;
    userRoute.phone = req.body.phone;
    userRoute.authCode = req.body.authCode;
    userRoute.authKey = req.body.authKey;
    userRoute.subscription = req.body.subscription;

// save the userRoute and check for errors
    userRoute.save(function (err) {
        // if (err)
        //     res.json(err);

res.json({
            message: 'New User created!',
            data: userRoute
        });
    });
};

// Handle view userRoute info
exports.view = function (req, res) {
    UserRoute.findOne({phone: req.params.userRoute_phone, authCode: req.params.userRoute_authCode}, function (err, userRoute) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'User details loading..',
            data: userRoute
        });
    });
};

// Handle authentication info
exports.auth = function (req, res) {
    UserRoute.findOne({_id: req.params.userRoute_id, authKey: req.params.userRoute_authKey}, function (err, userRoute) {
        if (err)
            res.send(err);
        res.json({
            data: userRoute
        });
    });
};


// Handle view userRoute info
exports.view = function (req, res) {
    UserRoute.findOne({phone: req.params.userRoute_phone}, function (err, userRoute) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'User details loading..',
            data: userRoute
        });
    });
};


// Handle update userRoute info
exports.update = function (req, res) {

UserRoute.findOne({phone: req.params.userRoute_phone}, function (err, userRoute) {
        if (err)
        res.send(err);
        userRoute.name = req.body.name ? req.body.name : userRoute.name;
        userRoute.phone = req.body.phone ? req.body.phone : userRoute.phone;
        userRoute.authCode = req.body.authCode ? req.body.authCode : userRoute.authCode;
        userRoute.authKey = req.body.authKey ? req.body.authKey : userRoute.authKey;
        userRoute.authorizedSchools = req.body.authorizedSchools ? req.body.authorizedSchools : userRoute.authorizedSchools;
        userRoute.subscription = req.body.subscription ? req.body.subscription : userRoute.subscription;

// save the userRoute and check for errors
        userRoute.save(function (err) {
            if (err)
            res.status(500).send(err);
            res.status(200).json({
                message: 'User Info updated',
                data: userRoute
            });
        });
    });
};

// Handle delete userRoute
exports.delete = function (req, res) {
    UserRoute.deleteOne({phone: req.params.userRoute_phone}, function (err, userRoute) {
      if (err)
      res.end(err);
      res.json({
            status: "success",
            message: 'User deleted'
        });
    });
};
