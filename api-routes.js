// Initialize express router
let router = require('express').Router();

// Set default API response
router.get('/', function (req, res) {
    res.json({
       status: 'API Its Working',
       message: 'You Are Not Allowed Here.',
    });
});

// Import contact controller
var userController = require('./userController');
var routeController = require('./routeController');
var schoolController = require('./schoolController');

// router.route('/contacts/:contact_id')
//     .get(contactController.view)
//     .patch(contactController.update)
//     .put(contactController.update)
//     .delete(contactController.delete);

// Route users
router.route('/users')
    .get(userController.index)
    .post(userController.new);

router.route('/users/auth/:userRoute_id/:userRoute_authKey')
    .get(userController.auth)

router.route('/users/:userRoute_phone')
    .get(userController.view)
    .patch(userController.update)
    .put(userController.update)
    .delete(userController.delete);


// Route (bus)routes
router.route('/routes')
    .get(routeController.index)
    .post(routeController.new);

router.route('/routes/school/:busRoute_school')
    .get(routeController.display)

router.route('/routes/:busRoute_code')
    .get(routeController.view)
    .patch(routeController.update)
    .put(routeController.update)
    .delete(routeController.delete);

// Route schools
router.route('/schools')
    .get(schoolController.index)
    .post(schoolController.new);

router.route('/schools/:schoolRoute_code')
    .get(schoolController.view)
    .patch(schoolController.update)
    .put(schoolController.update)
    .delete(schoolController.delete);

// // Route schools
// router.route('/sms')
//     .get(twilioController.index)
// router.route('/sms/:twilioRoute_code')
//     .post(twilioController.new);

// Export API routes
module.exports = router;
