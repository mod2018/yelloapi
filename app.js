let bodyParser = require("body-parser");
let mongoose = require('mongoose');

//Twilio Environmental Variables
process.env.accountSid = 'AC5cc50cffa57e5e12d47fff91f5a1ebc7';
process.env.authToken = 'b5507e605fa7186784b33469d2d05dbc';
const client = require('twilio')(process.env.accountSid, process.env.authToken);

// const MongoClient = require("mongodb").MongoClient;
// const ObjectId = require("mongodb").ObjectID;
//
CONNECTION_URL="mongodb+srv://mitchellDeYoung:sudoDrake1993@app-cluster-tiycb.mongodb.net/test?retryWrites=true"
// const DATABASE_NAME = "Yello";

// Import express
let express = require('express')
// Initialize the app
let app = express();
// Import routes
let apiRoutes = require("./api-routes")
const https = require('https');
const fs = require('fs');

const options = {
  key: fs.readFileSync('/etc/letsencrypt/live/api.yellobus.ca/privkey.pem'),
  cert: fs.readFileSync('/etc/letsencrypt/live/api.yellobus.ca/cert.pem')
};

// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

// Connect to Mongoose and set connection variable
mongoose.connect(CONNECTION_URL, { useNewUrlParser: true });

var db = mongoose.connection;
// Setup server port
var port = process.env.PORT || 8773;
const host = process.env.HOST || 'localhost';
//breaking error previously

// Send message for default URL
app.get('/', (req, res) => res.send('Invalid Request'));

app.post('/api/sms', function(req, res) {
    var number = req.body.number
    var code = req.body.code
    client.messages
      .create({
         body: 'Yello Login Code:' + code,
         from: '+12898137543 ',
         to: '+1' + number
       })
      .then(message => res.send(message));

});

// Use Api routes in the App
app.use('/api', apiRoutes)

// Launch app to listen to specified port
https.createServer(options, app)
.listen(port, function () {
    console.log("Running YelloAPI on port " + port);
})
