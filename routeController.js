// busRouteController.js

// Import busRoute model
BusRoute = require('./routeModel');

// Handle index actions
exports.index = function (req, res) {
    BusRoute.get(function (err, busRoute) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Routes retrieved successfully",
            data: busRoute
        });
    });
};

// Handle create busRoute actions
exports.new = function (req, res) {
    var busRoute = new BusRoute();
    busRoute.name = req.body.name;
    busRoute.school = req.body.school;
    busRoute.code = req.body.code;
    busRoute.active = req.body.active;
    busRoute.latitude = req.body.latitude;
    busRoute.longitude = req.body.longitude;

// save the busRoute and check for errors
    busRoute.save(function (err) {
        // if (err)
        //     res.json(err);

res.json({
            message: 'New Route created!',
            data: busRoute
        });
    });
};

// Handle view busRoute info
exports.view = function (req, res) {
    BusRoute.findOne({code: req.params.busRoute_code}, function (err, busRoute) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Route details loading..',
            data: busRoute
        });
    });
};

// Handle view busRoute info
exports.display = function (req, res) {
    BusRoute.find({school: req.params.busRoute_school}, function (err, busRoute) {
        if (err)
            res.send(err);
        res.json({
            data: busRoute
        });
    });
};

// Handle update busRoute info
exports.update = function (req, res) {

BusRoute.findOne({code: req.params.busRoute_code}, function (err, busRoute) {
        if (err)
        res.send(err);
        busRoute.name = req.body.name ? req.body.name : busRoute.name;
        busRoute.school = req.body.school ? req.body.school : busRoute.school;
        busRoute.code = req.body.code ? req.body.code : busRoute.code;
        busRoute.active = req.body.active ? req.body.active : busRoute.active;
        busRoute.latitude = req.body.latitude ? req.body.latitude : busRoute.latitude;
        busRoute.longitude = req.body.longitude ? req.body.longitude : busRoute.longitude;



// save the busRoute and check for errors
        busRoute.save(function (err) {
            if (err)
            res.status(500).send(err);
            res.status(200).json({
                message: 'Route Info updated',
                data: busRoute
            });
        });
    });
};

// Handle delete busRoute
exports.delete = function (req, res) {
    BusRoute.remove({
        _id: req.params.busRoute_id
    }, function (err, busRoute) {
        if (err)
            res.end(err);

res.json({
            status: "success",
            message: 'Route deleted'
        });
    });
};
