// routeModel.js
var mongoose = require('mongoose');

// Setup schema
var schoolSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    code: {
        type: String,
        required: true
    },
    create_date: {
        type: Date,
        default: Date.now
    }
});

// Export String model
var SchoolRoute = module.exports = mongoose.model('schoolRoute', schoolSchema);

module.exports.get = function (callback, limit) {
    SchoolRoute.find(callback).limit(limit);
}
